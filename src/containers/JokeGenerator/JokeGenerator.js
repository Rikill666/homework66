import React, {Component} from 'react';
import JokeBlock from "../../components/JokeBlock/JokeBlock";
import axiosOrder from "../../axiosOrders";
import spinnerHandler from "../../hoc/spinnerHandler";

class JokeGenerator extends Component {
    state = {
        jokes: []
    };

    async jokesGenerations(how){
        const promiseArray = [];
        for(let i = 0; i < how; i++){
            const response = await axiosOrder("/");
            promiseArray.push(response);
        }
        let jokes = await Promise.all(promiseArray.map(function (some) {
            return some.data;
        }));
        this.setState({jokes});
    };

    async componentDidMount() {
        this.jokesGenerations(1);
    }

    render() {
        return (
            <div>
                <JokeBlock jokes={this.state.jokes} newJokes={()=>this.jokesGenerations(5)}/>
            </div>
        );
    }
}

export default spinnerHandler(JokeGenerator, axiosOrder);