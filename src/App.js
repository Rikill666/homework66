import React from 'react';
import JokeGenerator from "./containers/JokeGenerator/JokeGenerator";
import {BrowserRouter, Switch, Route} from "react-router-dom";

const App = () => (
    <BrowserRouter>
            <Switch>
                <Route path="/" exact component={JokeGenerator}/>
                <Route render={() => <h1>Not Found</h1>}/>
            </Switch>
    </BrowserRouter>
);

export default App;
