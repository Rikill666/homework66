import React, {Component, Fragment} from 'react';
import Backdrop from "../components/UI/Backdrop/Backdrop";

const spinnerHandler = (WrapperComponent, axios) =>{
    return class extends Component {
        constructor(props) {
            super(props);

            this.state = {
                spinner: false
            };

            axios.interceptors.request.use(req => {
                this.setState({spinner:true});
                return req;
            });

            axios.interceptors.response.use(res => {
                this.setState({spinner:false});
                return res;
            }, err => {
                this.setState({spinner:false});
                throw err;
            });
        }
        render() {
            return(
                <Fragment>
                    <WrapperComponent {...this.props}/>
                    <Backdrop show={this.state.spinner}/>
                </Fragment>
            );
        }
    }
};
export default spinnerHandler;