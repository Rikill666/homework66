import React from 'react';
import "./Spinner.css";
import Loader from '../../../images/805.gif'

const Spinner = () => {
    return (
        <div className="Spinner">
            <img src={Loader} alt=""/>
        </div>
    );
};


export default Spinner;